package heranca.baralho;

public class Carta {
	protected String naipe;
	protected String rank;
	
	public Carta(String rank, String naipe) {
		this.naipe = naipe;
		this.rank = rank;
	}
	
	public String getNaipe() {
		return naipe;
	}
	
	public String getRank() {
		return rank;
	}
}
