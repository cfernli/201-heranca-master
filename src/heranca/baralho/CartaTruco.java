package heranca.baralho;

public class CartaTruco extends Carta{

	public CartaTruco(String rank, String naipe){
		super(rank, naipe);
		
//		if(rank == "Oito" || rank == "Nove" || rank == "Dez") {
//			throw new Exception("Valor inválido para rank");
//		}	
	}
	
	@Override
	public String getNaipe() {
		return "Paus";
	}

	public int getValorNaipe() {
		switch (naipe) {
			case "Paus":
				return 4;
			case "Copas":
				return 3;
	
			default:
				return 0;
			}
	}
}
